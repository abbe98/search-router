# Search Router

*Personal prototype*

Search Router is a simple server intended to sit in between you and your search engine. It matches your query against a list of regular expressions and redirects you to a different search engine if it matches. This way your default generic search engine only sees the queries that don't match any of your rules.
