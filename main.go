package main

import (
	"net/http"

	"codeberg.org/abbe98/search-router/matchrules"
	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()
	r.GET("/", func(c *gin.Context) {
		searchQuery := c.Query("query")

		if searchQuery == "" {
			c.JSON(200, gin.H{
				"message": "No query",
			})
		}

		match := matchrules.Rules.Match(searchQuery)
		if match != nil {
			if match.PreserveTrigger {
				c.Redirect(http.StatusFound, match.Target+searchQuery)
			}

			c.Redirect(http.StatusFound, match.Target+match.TransformedQuery)
		}

		c.Redirect(http.StatusFound, "https://duckduckgo.com/?q="+searchQuery)
	})

	r.GET("/rules", func(c *gin.Context) {
		c.JSON(200, matchrules.Rules)
	})

	r.Run()
}
