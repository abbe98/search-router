package matchrules

import (
	"regexp"
)

// MatchRule is a rule that can be used to match query patterns
type MatchRule struct {
	// Name is the name of the rule
	Name string
	// Pattern is the pattern to match
	Pattern string
	// Regex is the compiled regex
	regex *regexp.Regexp
	// Target is the target to redirect to
	Target string
	// PreserveTrigger is whether to preserve the trigger in the query
	PreserveTrigger bool
	// TransformedQuery is the query after the trigger has been removed
	TransformedQuery string
}

// MatchRules is a list of MatchRule
type MatchRules []MatchRule

// Match returns the first matching rule
func (m MatchRules) Match(query string) *MatchRule {
	for _, rule := range m {
		if rule.regex.MatchString(query) {
			if !rule.PreserveTrigger {
				rule.TransformedQuery = rule.regex.ReplaceAllString(query, "")
			}

			return &rule
		}
	}
	return nil
}

// NewMatchRules creates a new MatchRules from a list of MatchRule
func NewMatchRules(rules []MatchRule) MatchRules {
	for i := range rules {
		rules[i].regex = regexp.MustCompile(rules[i].Pattern)
	}
	return rules
}

var Rules = NewMatchRules([]MatchRule{
	{
		Name:            "DuckDuckGo",
		Pattern:         `^d `,
		Target:          "https://duckduckgo.com/?q=",
		PreserveTrigger: false,
	},
	{
		Name:            "Stack Overflow",
		Pattern:         `python|golang|html|css|rust|vala|javascript|django`,
		Target:          "https://stackoverflow.com/search?q=",
		PreserveTrigger: true,
	},
	{
		Name:            "Recipes",
		Pattern:         " recept$",
		Target:          "https://www.ica.se/recept/?q=",
		PreserveTrigger: false,
	},
	{
		Name:            "Wikipedia (sv)",
		Pattern:         "^w ",
		Target:          "https://sv.wikipedia.org/w/index.php?ns0=1&search=",
		PreserveTrigger: false,
	},
	{
		Name:            "wikidata",
		Pattern:         "^wd ",
		Target:          "https://www.wikidata.org/w/index.php?&ns0=1&ns120=1&search=",
		PreserveTrigger: false,
	},
	{
		Name:            "Wikipedia (en)",
		Pattern:         "^we ",
		Target:          "https://en.wikipedia.org/w/index.php?ns0=1&search=",
		PreserveTrigger: false,
	},
})
